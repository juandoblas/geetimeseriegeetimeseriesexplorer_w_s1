from hubdsm.core.gdalraster import GdalRaster

from osgeo import gdal
gdal.SetConfigOption('GDAL_VRT_ENABLE_PYTHON', 'YES')
bandNames = ['NIR', 'SW1', 'RED']

raster = GdalRaster.open('zeros2.vrt')
print(raster.readAsArray())