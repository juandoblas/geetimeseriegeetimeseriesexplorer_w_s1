from os.path import join

from hubdsm.core.gdalraster import GdalRaster

gdalRaster = GdalRaster.open(r'C:\test\daily_.tif')
dates = gdalRaster.metadataItem('dates', 'TIMESERIES')
dates = dates[:92] # fix dates!!!
names = gdalRaster.metadataItem('names', 'TIMESERIES')
print(dates)
print(len(dates))
array4d = gdalRaster.readAsArray()
zsize, ysize, xsize = array4d.shape



array4d = array4d.reshape((len(names), len(dates), ysize, xsize))
for i, date in enumerate(dates):
    array = array4d[:, i]
    r = GdalRaster.createFromArray(array=array, grid=gdalRaster.grid, filename=rf'c:\test\eotsv\chip_{date}')
    for b, name in zip(r.bands, names):
        b.setDescription(name)
