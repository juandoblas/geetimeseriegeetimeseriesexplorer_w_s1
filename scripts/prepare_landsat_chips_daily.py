import time
import ee
import numpy as np
from PyQt5.QtCore import QDate
from osgeo import ogr

from hubdsm.core.gdalraster import GdalRaster
from hubdsm.core.geometry import Geometry
from hubdsm.core.location import Location
from hubdsm.core.ogrdriver import OgrDriver

bandNames = ['BLUE', 'GREEN', 'RED', 'NIR', 'SWIR1', 'SWIR2']

arrays = dict()
for collection in ['LT04', 'LT05', 'LE07', 'LC08']:
    raster = GdalRaster.open(rf'c:\test\chipsAllLandsat\{collection}.tif')
    for band in raster.bands:
        array = band.readAsArray().astype(np.float32)
        array[array == -9999] = np.nan
        arrays[band.description] = array
        ysize, xsize = array.shape


# create monthly mean
years = list(range(1984, 2021))
bigarray = np.full((len(bandNames), ysize * len(years), xsize * 366), -9999)

for iyear, year in enumerate(years):
    ix = -1
    for imonth, month in enumerate(range(1, 13)):
        for iday, day in enumerate(range(1, 32)):
            ix += 1
            date = QDate(year, month, day)
            if not date.isValid():
                continue
            for iband, bandName in enumerate(bandNames):
                keys = [key for key in arrays if key.endswith(bandName) and key[12:20] == date.toString('yyyyMMdd')]
                if len(keys) == 0:
                    print('skip', year, month, day)
                    continue
                else:
                    print(date)
                result = np.nanmean([arrays[key] for key in keys], axis=0)
                result[np.isnan(result)] = -9999
                bigarray[iband, iyear * ysize: (iyear + 1) * ysize, ix * xsize:(ix + 1) * xsize] = result

raster = GdalRaster.createFromArray(bigarray, filename=r'c:\test\chipsAllLandsat\collage\chips_daily_mean.tif')
for band, bandName in zip(raster.bands, bandNames):
    band.setNoDataValue(-9999)
    band.setDescription(bandName)

raster = GdalRaster.open(r'c:\test\chipsAllLandsat\collage\chips_daily_mean.tif')

size = raster.grid.extent.size

layer = OgrDriver('GPKG').createVector(
    r'c:\test\chipsAllLandsat\collage\label_daily.gpkg').createLayer('labels', geometryType=ogr.wkbPoint
)
layer.createField('datestamp', ogr.OFTString)
for iyear, year in enumerate(reversed(range(1984, 2021))):
    ix = -1
    for month in range(1, 13):
        for day in range(1, 32):
            ix += 1
            date = QDate(year, month, day)
            if not date.isValid():
                continue
            nyears = 2020 - 1984 + 1
            x = size.x / 366 * ix + size.x / 366 * 0.1
            y = size.y / nyears * iyear + size.y / nyears *0.1
            layer.createFeature(Geometry.fromLocation(Location(x, y)), datestamp=date.toString('yyyy-MM-dd'))
