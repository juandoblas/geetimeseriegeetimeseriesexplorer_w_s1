import time
import ee
import numpy as np
from osgeo import ogr

from hubdsm.core.gdalraster import GdalRaster
from hubdsm.core.geometry import Geometry
from hubdsm.core.location import Location
from hubdsm.core.ogrdriver import OgrDriver

bandNames = ['BLUE', 'GREEN', 'RED', 'NIR', 'SWIR1', 'SWIR2']

arrays = dict()
for collection in ['LT04', 'LT05', 'LE07', 'LC08']:
    raster = GdalRaster.open(rf'c:\test\chipsAllLandsat\{collection}.tif')
    for band in raster.bands:
        array = band.readAsArray().astype(np.float32)
        array[array == -9999] = np.nan
        arrays[band.description] = array
        ysize, xsize = array.shape

# create monthly mean
years = list(range(1983, 2021))
bigarray = np.full((len(bandNames), ysize * len(years), xsize * 12), -9999)
for iyear, year in enumerate(years):
    for iquarter, startmonth in enumerate((1, 4, 7, 10)):
        for iband, bandName in enumerate(bandNames):
            keys = [key for key in arrays if key.endswith(bandName) and (
                    key[12:18] == str(year) + str(startmonth).zfill(2) or key[12:18] == str(year) + str(startmonth+1).zfill(2) or key[12:18] == str(year) + str(startmonth+2).zfill(2)
            )]
            if len(keys) == 0:
                print('skip', year, startmonth)
                continue
            else:
                print(year, startmonth)
            result = np.nanmean([arrays[key] for key in keys], axis=0)
            result[np.isnan(result)] = -9999
            bigarray[iband, iyear * ysize: (iyear + 1) * ysize, iquarter * xsize:(iquarter + 1) * xsize] = result

raster = GdalRaster.createFromArray(bigarray, filename=r'c:\test\chipsAllLandsat\collage\chips_quarterly_mean.tif')
for band, bandName in zip(raster.bands, bandNames):
    band.setNoDataValue(-9999)
    band.setDescription(bandName)


#raster = GdalRaster.open(r'c:\test\chipsAllLandsat\collage\chips_mean.tif')

size = raster.grid.extent.size

layer = OgrDriver('GPKG').createVector(
    r'c:\test\chipsAllLandsat\collage\labels_quarterly.gpkg').createLayer('labels', geometryType=ogr.wkbPoint
)
layer.createField('datestamp', ogr.OFTString)
for iyear, year in enumerate(reversed(range(1984, 2021))):
    for iquarter, label in enumerate(['I', 'II', 'III','IV']):
        nyears = 2021 - 1983
        x = size.x / 12 * iquarter + size.x / 12 * 0.1
        y = size.y / nyears * iyear + size.y / nyears *0.1
        layer.createFeature(Geometry.fromLocation(Location(x, y)), datestamp=f'{year}-{iquarter}')
