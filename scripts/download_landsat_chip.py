import time
import ee
import numpy as np

from hubdsm.core.extent import Extent
from hubdsm.core.gdalraster import GdalRaster
from hubdsm.core.grid import Grid
from hubdsm.core.location import Location
from hubdsm.core.projection import Projection
from hubdsm.core.resolution import Resolution
from hubdsm.core.size import Size

ee.Initialize()


import ee

bandNames = ['BLUE', 'GREEN', 'RED', 'NIR', 'SWIR1', 'SWIR2']

def getCollections():
    # merge all Landsat collections
    l4 = ee.ImageCollection('LANDSAT/LT04/C01/T1_SR').map(filterQuality).select(['B1', 'B2', 'B3', 'B4', 'B5', 'B7'], bandNames)
    l5 = ee.ImageCollection('LANDSAT/LT05/C01/T1_SR').map(filterQuality).select(['B1', 'B2', 'B3', 'B4', 'B5', 'B7'], bandNames)
    l7 = ee.ImageCollection('LANDSAT/LE07/C01/T1_SR').map(filterQuality).select(['B1', 'B2', 'B3', 'B4', 'B5', 'B7'], bandNames)
    l8 = ee.ImageCollection('LANDSAT/LC08/C01/T1_SR').map(filterQuality).select(['B2', 'B3', 'B4', 'B5', 'B6', 'B7'], bandNames)
    return l4, l5, l7, l8

# filter quality
def filterQuality(image):
    # filter cloud shadows
    image = image.updateMask(image.select('pixel_qa').rightShift(3).bitwiseAnd(1).eq(0))
    # filter clouds
    image = image.updateMask(image.select('pixel_qa').rightShift(5).bitwiseAnd(1).eq(0))
    return image

t0 = time.time()
point = ee.Geometry.Point(13.411445788747644, 52.518666177654396)  # over Berlin
circle = point.buffer(30*50).bounds()

for collection in getCollections():
    collection = collection.filterBounds(circle)
    collectionId = collection.getInfo()['id'].split('/')[1]

    grids = list()
    arrays = list()
    ids = list()
    for bandName in bandNames:
        image = collection.select(bandName).toBands().clip(circle).updateMask(ee.Image(1))
        print(image.select(0).getDownloadURL({}))
        imageInfo = image.getInfo()
        sample = image.sampleRectangle(circle, [], -9999).getInfo()

        for band in imageInfo['bands']:
            id = band['id']
            print(id)
            epsg = int(band['crs'].split(':')[1])
            xres, _, xmin, _, yres, ymax = band['crs_transform']

            xsize, ysize = tuple(band['dimensions'])
            xoff, yoff = band['origin']

            array = np.array(sample['properties'][band['id']])
            assert (ysize, xsize) == array.shape

            resolution = Resolution(x=xres, y=abs(yres))
            extent = Extent(ul=Location(x=xmin+xoff*xres, y=ymax+yoff*yres), size=Size(x=xsize*xres, y=ysize*abs(yres)))
            grid = Grid(extent=extent, resolution=resolution, projection=Projection.fromEpsg(epsg=epsg))

            grids.append(grid)
            ids.append(id)
            arrays.append(array)

    for grid in grids:
        assert grid.equal(grids[0])

    raster = GdalRaster.createFromArray(array=np.array(arrays), grid=grids[0], filename=rf'c:\test\chipsAllLandsat\{collectionId}.tif')
    for band, id in zip(raster.bands, ids):
        band.setDescription(id)

    #    i += 1
     #   if i > 10:
      #      exit()

    #sample = .getInfo()
    #for name, arraysample['properties']
    #print(sample)

print('Done in', time.time() - t0, 'sec')