import random
import time
from multiprocessing.pool import Pool, ThreadPool
from os.path import basename, join
from typing import Dict

import ee
import numpy as np
from PyQt5.QtCore import QDate
from qgis._core import QgsVectorLayer, QgsFeature, QgsGeometry, QgsPointXY

from hubdc.core import RasterDataset, Grid, Extent, Resolution, Projection

ee.Initialize()


def downloadRoiChipCollection(args):
    imageCollection, roi, pixel, noData = args
    imageCollection = imageCollection.filterBounds(roi)

    chips = list()

    nimages = imageCollection.size().getInfo()
    for iimage in range(nimages):
        image = ee.Image(imageCollection.toList(imageCollection.size()).get(iimage))
        chips.append(downloadRoiChipImage((image, roi, pixel, noData)))

    return chips


def downloadRoiChipImage(args):
    image, roi, pixel, noData = args

    image = image.clipToBoundsAndScale(geometry=roi, width=pixel, height=pixel)
    bandNames = image.bandNames().getInfo()
    print('read chip', bandNames)

    sample = image.sampleRectangle(roi, None, noData).getInfo()
    array = np.array([sample['properties'][bandName] for bandName in bandNames])
    _, ysize, xsize = array.shape
    geometry = image.geometry().getInfo()
    epsg = int(geometry['crs']['properties']['name'].split(':')[-1])
    x, y = np.array(geometry['coordinates'])[0].T
    resolution = Resolution((max(x) - min(x)) / xsize, (max(y) - min(y)) / ysize)
    extent = Extent(min(x), max(x), min(y), max(y), projection=Projection.fromEpsg(epsg))
    grid = Grid(extent=extent, resolution=resolution)
    return array, bandNames, grid


def downloadMultipleRoiChip(imageCollection, rois, pixel, noData):
    argss = list()
    for i, roi in enumerate(rois):
        args = (imageCollection, roi, pixel, noData)
        argss.append(args)

    if 0:
        for args in argss:
            downloadRoiChipCollection(args)
    else:
        pool = Pool(30)
        pool.map(downloadRoiChipCollection, argss)


def testRoi():
    point = ee.Geometry.Point(13.411445788747644, 52.518666177654396)  # over Berlin
    pixel = 200
    roi = point.buffer(1000).bounds()
    noData = -9999
    imageCollection = (
        ee.ImageCollection('LANDSAT/LC08/C01/T1_SR')
            .filterMetadata('CLOUD_COVER', 'less_than', 1)
            .select(['B2', 'B3', 'B4', 'B5', 'B6', 'B7'], ['BLUE', 'GREEN', 'RED', 'NIR', 'SWIR1', 'SWIR2'])
            .filterDate('2019-09-01', '2020-01-01')
    )

    downloadRoiChip((imageCollection, roi, pixel, noData, r'c:\test\singleRoi'))


def usecaseYearlyMeanRoi():
    point = ee.Geometry.Point(13.411445788747644, 52.518666177654396)  # over Berlin
    pixel = 200
    roi = point.buffer(1000).bounds()
    noData = -9999

    imageCollection = (
        ee.ImageCollection('LANDSAT/LC08/C01/T1_SR')
            .filterMetadata('CLOUD_COVER', 'less_than', 30)
            .select(['B5', 'B6', 'B4'], ['NIR', 'SWIR1', 'RED'])
    )

    years = range(2014, 2021)
    dates = '{' + ', '.join([f'{y}-01-01' for y in years])+'}'
    chipsCollection = ee.ImageCollection.fromImages(
        [imageCollection.filterDate(f'{y}-01-01', f'{y+1}-01-01').reduce(ee.Reducer.mean()) for y in years]
    )

    chips = downloadRoiChipCollection((chipsCollection, roi, pixel, noData))

    array = list()
    bandNames = list()
    for array_, bandNames_, grid in chips:
        array.extend(array_)
        bandNames.extend(bandNames_)

    rasterDataset = RasterDataset.fromArray(array=np.array(array), grid=grid, filename=r'c:\test\yearly.tif')
    for band, bandName in zip(rasterDataset.bands(), bandNames):
        band.setDescription(bandName)
    rasterDataset.setMetadataItem('names', '{NIR, SWIR1, RED}', 'TIMESERIES')
    rasterDataset.setMetadataItem('dates', dates, 'TIMESERIES')

def usecaseMonthlyMeanRoi():
    point = ee.Geometry.Point(13.411445788747644, 52.518666177654396)  # over Berlin
    pixel = 200
    roi = point.buffer(1000).bounds()
    noData = -9999

    imageCollection = (
        ee.ImageCollection('LANDSAT/LC08/C01/T1_SR')
            .filterBounds(roi)
            .filterMetadata('CLOUD_COVER', 'less_than', 30)
            .select(['B5', 'B6', 'B4'], ['NIR', 'SWIR1', 'RED'])
    )

    starts = list()
    ends = list()
    for y in range(2020, 2021):
        for m in range(1, 13):
            date = QDate(y, m, 1)
            starts.append(date.toString('yyyy-MM-dd'))
            ends.append(date.addMonths(1).toString('yyyy-MM-dd'))

    images = list()
    for start, end in zip(starts, ends):
        print(start, end, flush=True)
        image = imageCollection.filterDate(start, end).reduce(ee.Reducer.mean())
        images.append(image)

        #images.append(imageCollection.reduce(ee.Reducer.mean()))

    chipsCollection = ee.ImageCollection.fromImages(images)

    chips = downloadRoiChipCollection((chipsCollection, roi, pixel, noData))

    array = list()
    bandNames = list()
    dates = list()
    for (array_, bandNames_, grid), date in zip(chips, starts):
        if array_ is None:
            print('skip', date)
            continue
        dates.append(date)
        array.extend(array_)
        bandNames.extend(bandNames_)

    rasterDataset = RasterDataset.fromArray(array=np.array(array), grid=grid, filename=r'c:\test\monthly.tif')
    for band, bandName in zip(rasterDataset.bands(), bandNames):
        band.setDescription(bandName)
    rasterDataset.setMetadataItem('names', '{NIR, SWIR1, RED}', 'TIMESERIES')
    rasterDataset.setMetadataItem('dates', '{' + ', '.join(dates) + '}', 'TIMESERIES')

    # r'c:\test\singleRoiYearly')

def usecaseDailyMeanRoi():
    point = ee.Geometry.Point(13.411445788747644, 52.518666177654396)  # over Berlin
    pixel = 200
    roi = point.buffer(1000).bounds()
    noData = -9999

    imageCollection = (
        ee.ImageCollection('LANDSAT/LC08/C01/T1_SR')
            .filterBounds(roi)
            #.filterMetadata('CLOUD_COVER', 'less_than', 30)
            .select(['B5', 'B6', 'B4'], ['NIR', 'SWIR1', 'RED'])
    )

    ####
    dates = imageCollection.toList(imageCollection.size()).map(lambda image: ee.Date(ee.Image(image).get('system:time_start')).format('YYYY-MM-dd')).getInfo()
    dates = list(sorted(set(dates)))
    images = list()
    for date in dates:
        y, m, d = map(int, date.split('-'))
        start = date
        end = QDate(y, m, d).addDays(1).toString('yyyy-MM-dd')
        image = imageCollection.filterDate(start, end).reduce(ee.Reducer.mean())
        images.append(image)
        print(date)

    chipsCollection = ee.ImageCollection.fromImages(images)
    chips = downloadRoiChipCollection((chipsCollection, roi, pixel, noData))

    array = list()
    bandNames = list()
    grid = None
    for chip in chips:
        array_, bandNames_, grid = chip
        array.extend(array_)
        bandNames.extend(bandNames_)

    rasterDataset = RasterDataset.fromArray(array=np.array(array), grid=grid, filename=r'c:\test\dailyALL.tif')
    for band, bandName in zip(rasterDataset.bands(), bandNames):
        band.setDescription(bandName)
    rasterDataset.setMetadataItem('names', '{NIR, SWIR1, RED}', 'TIMESERIES')
    rasterDataset.setMetadataItem('dates', '{' + ', '.join(dates) + '}', 'TIMESERIES')

    # r'c:\test\singleRoiYearly')


def testMultipleRoi():
    points = [ee.Geometry.Point(13.5 + random.random(), 52.5 + random.random()) for _ in range(10)]
    pixel = 200
    rois = [point.buffer(1000).bounds() for point in points]
    noData = -9999
    imageCollection = (
        ee.ImageCollection('LANDSAT/LC08/C01/T1_SR')
            .filterMetadata('CLOUD_COVER', 'less_than', 1)
            .select(['B2', 'B3', 'B4', 'B5', 'B6', 'B7'], ['BLUE', 'GREEN', 'RED', 'NIR', 'SWIR1', 'SWIR2'])
            .filterDate('2019-01-01', '2020-01-01')
    )

    downloadMultipleRoiChip(imageCollection=imageCollection, rois=rois, pixel=pixel, noData=noData)


def testMultipleRoiFromVector():
    layer = QgsVectorLayer(r'C:\Work\data\gms\lucas\lucasWgs84.gpkg')
    points = list()
    for feature in layer.getFeatures():
        assert isinstance(feature, QgsFeature)
        point = feature.geometry().asPoint()
        points.append(ee.Geometry.Point(point.x(), point.y()))
        if len(points) == 100:
            break

    pixel = 200
    rois = [point.buffer(1000).bounds() for point in points]
    noData = -9999
    imageCollection = (
        ee.ImageCollection('LANDSAT/LC08/C01/T1_SR')
            .filterMetadata('CLOUD_COVER', 'less_than', 1)
            .select(['B2', 'B3', 'B4', 'B5', 'B6', 'B7'], ['BLUE', 'GREEN', 'RED', 'NIR', 'SWIR1', 'SWIR2'])
        # .filterDate('2019-01-01', '2020-01-01')
    )

    downloadMultipleRoiChip(imageCollection=imageCollection, rois=rois, pixel=pixel, noData=noData,
        outdir=r'c:\test\multiple')


if __name__ == '__main__':
    t0 = time.time()
    # testRoi()
    testMultipleRoi()
    # testMultipleRoiFromVector()
    #usecaseYearlyMeanRoi()
    #usecaseMonthlyMeanRoi()
    #usecaseDailyMeanRoi()

    print('Done in', time.time() - t0, 'sec')
