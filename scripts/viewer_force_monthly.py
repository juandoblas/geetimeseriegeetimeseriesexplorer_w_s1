# see https://qgis-docs.readthedocs.io/en/latest/docs/pyqgis_developer_cookbook/canvas.html

from collections import defaultdict

import sys
from os.path import join

from PyQt5.QtCore import QDate
from PyQt5.QtWidgets import QMainWindow, QWidget, QGridLayout, QPushButton, QApplication, QLabel
from qgis._core import QgsVectorLayer, QgsProject, QgsRasterLayer, QgsMultiBandColorRenderer, QgsContrastEnhancement
from qgis._gui import QgsMapCanvas, QgsMapToolPan

from hubdsm.core.gdalraster import GdalRaster

from osgeo import gdal
gdal.SetConfigOption('GDAL_VRT_ENABLE_PYTHON', 'YES')
bandNames = ['NIR', 'SW1', 'RED']

bands = defaultdict(list)

root = r'C:\Work\data\FORCE\TSS'
root = r'\\141.20.140.91\NAS_Rodinia\Grassland\lower_saxony_sentinel2_TSA_coreg\X0061_Y0046'
filenames = (
join(root, '2018-2020_001-365_HL_TSA_SEN2L_NIR_TSS.tif'),
join(root, '2018-2020_001-365_HL_TSA_SEN2L_SW1_TSS.tif'),
join(root, '2018-2020_001-365_HL_TSA_SEN2L_RED_TSS.tif')
)

for filename in filenames:
    raster = GdalRaster.open(filename)
    for band in raster.bands:
        bandName = band.metadataDomain('FORCE')['Domain']
        yearmonth = ''.join(band.metadataDomain('FORCE')['Date'].split('-')[:2])
        bands[(yearmonth, bandName)].append((raster.filename, band.number))

years = list(range(2018, 2029))
months = list(range(1, 13))[5:7]

with open('mean.vrt', 'w') as f:
    f.write(
    '''<VRTDataset rasterXSize="3000" rasterYSize="3000">
  <SRS dataAxisToSRSAxisMapping="2,1">PROJCS["ETRS89-extended / LAEA Europe",GEOGCS["ETRS89",DATUM["European_Terrestrial_Reference_System_1989",SPHEROID["GRS 1980",6378137,298.257222101,AUTHORITY["EPSG","7019"]],AUTHORITY["EPSG","6258"]],PRIMEM["Greenwich",0,AUTHORITY["EPSG","8901"]],UNIT["degree",0.0174532925199433,AUTHORITY["EPSG","9122"]],AUTHORITY["EPSG","4258"]],PROJECTION["Lambert_Azimuthal_Equal_Area"],PARAMETER["latitude_of_center",52],PARAMETER["longitude_of_center",10],PARAMETER["false_easting",4321000],PARAMETER["false_northing",3210000],UNIT["metre",1,AUTHORITY["EPSG","9001"]],AXIS["Northing",NORTH],AXIS["Easting",EAST],AUTHORITY["EPSG","3035"]]</SRS>
  <GeoTransform>  4.2860263630416505e+06,  1.0000000000000000e+01,  0.0000000000000000e+00,  3.1949196079648044e+06,  0.0000000000000000e+00, -1.0000000000000000e+01</GeoTransform>
    ''')

    vrtBandNumber = 0
    for year in years:
        for month in months:
            for bandName in bandNames:
                vrtBandNumber += 1
                yearmonth = str(year) + str(month).zfill(2)
                f.write(
rf'''
    <VRTRasterBand dataType="Int16" band="{vrtBandNumber}" subClass="VRTDerivedRasterBand">
        <Description>{yearmonth + '_' + bandName}</Description>
        <NoDataValue>-9999</NoDataValue>
''')
                for filename, sourceBandNumber in bands[yearmonth,bandName]:
                    f.write(
rf'''
        <SimpleSource>
            <SourceFilename relativeToVRT="0" shared="0">{filename}</SourceFilename>
            <SourceBand>{sourceBandNumber}</SourceBand>
            <SourceProperties RasterXSize="3000" RasterYSize="3000" DataType="Int16" BlockXSize="3000" BlockYSize="300" />
            <SrcRect xOff="0" yOff="0" xSize="3000" ySize="3000" />
            <DstRect xOff="0" yOff="0" xSize="3000" ySize="3000" />
        </SimpleSource>
''')
                f.write(
'''
        <PixelFunctionLanguage>Python</PixelFunctionLanguage>
        <PixelFunctionType>geetemporalprofileplotting.vrtpixelfunction.mean</PixelFunctionType>
        </VRTRasterBand>
''')

    f.write(
'''
</VRTDataset>
''')

#raster.translate(filename='test.vrt', bandList=[1])
#print(GdalRaster.open('mean.vrt').readAsArray().shape)
#exit()

app = QApplication(sys.argv)

#layer = QgsVectorLayer(r'C:/QGIS/apps/qgis/resources/data/world_map.gpkg|layername=countries', baseName='world_map')
#layer = QgsRasterLayer(r'mean.vrt', baseName='mean')
#QgsProject.instance().addMapLayers([layer])

mainWindow = QMainWindow()
mainWindow.resize(800, 600)
widget = QWidget()
layout = QGridLayout()

for i, year in enumerate(years):
    layout.addWidget(QLabel(str(year)), i + 1, 0)

for i, month in enumerate(months):
    layout.addWidget(QLabel(QDate(2000, month, 1).toString('MMM')), 0, i + 1)

ds: gdal.Dataset = gdal.Open(r'mean.vrt')
i = -1

mapCanvases = list()

for iyear, year in enumerate(years):
    for imonth, month in enumerate(months):
        yearmonth = str(year) + str(month).zfill(2)
        for iband in range(ds.RasterCount):
            bandName = ds.GetRasterBand(iband+1).GetDescription()
            if bandName == yearmonth + '_' + 'NIR':
                redBand = iband + 1
            if bandName == yearmonth + '_' + 'SW1':
                greenBand = iband + 1
            if bandName == yearmonth + '_' + 'RED':
                blueBand = iband + 1

        layer = QgsRasterLayer(r'mean.vrt', baseName='mean')
        renderer: QgsMultiBandColorRenderer = layer.renderer()
        renderer.setRedBand(redBand)
        renderer.setGreenBand(greenBand)
        renderer.setBlueBand(blueBand)
        renderer.redContrastEnhancement().setMinimumValue(0)
        renderer.redContrastEnhancement().setMaximumValue(3000)
        renderer.greenContrastEnhancement().setMinimumValue(0)
        renderer.greenContrastEnhancement().setMaximumValue(3000)
        renderer.blueContrastEnhancement().setMinimumValue(0)
        renderer.blueContrastEnhancement().setMaximumValue(3000)

        QgsProject.instance().addMapLayers([layer])
        mapCanvas = QgsMapCanvas()
        mapCanvas.setLayers([layer])
        mapCanvas.setDestinationCrs(layer.crs())
        mapCanvas.setExtent(layer.extent())
        layout.addWidget(mapCanvas, iyear + 1, imonth +1)
        mapTool = QgsMapToolPan(mapCanvas)
        mapCanvas.setMapTool(mapTool)
        mapTool.activate()
        mapCanvases.append(mapCanvas)


def onExtentsChanged():
    extent = mapCanvases[0].extent()
    for mapCanvas in mapCanvases[1:]:
        mapCanvas.setExtent(extent)
        mapCanvas.refresh()

mapCanvases[0].extentsChanged.connect(onExtentsChanged)



widget.setLayout(layout)
mainWindow.setCentralWidget(widget)
mainWindow.show()

sys.exit(app.exec_())
